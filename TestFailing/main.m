//
//  main.m
//  TestFailing
//
//  Created by Andrew Page on 7/10/14.
//  Copyright (c) 2014 ShipIO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IOAppDelegate class]));
    }
}
