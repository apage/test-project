//
//  IOAppDelegate.h
//  TestFailing
//
//  Created by Andrew Page on 7/10/14.
//  Copyright (c) 2014 ShipIO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
